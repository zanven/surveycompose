package main

import (
	"fmt"
	"os"

	"github.com/zanven42/survey"
	"gitlab.com/zanven/surveycompose"
)

type FieldParams struct {
	Name string
	Type string
}
type StructParams struct {
	Name   string
	Fields []FieldParams
}

func main() {
	file, err := os.Open("survey.yml")
	if err != nil {
		fmt.Println("Can not open file,", err)
		return
	}
	values, err := surveycompose.New(file)
	if err != nil {
		fmt.Println("can not new compose", err)
		os.Exit(1)
	}
	// e := struct {
	// 	Testcall    string
	// 	Secondcall  string
	// 	Multiselect []string
	// 	Boolconfirm bool
	// }{Testcall: "testcalldvbk", Secondcall: "string4", Multiselect: []string{"string2", "string4"}}
	e := make(map[string]interface{})
	e["testcall"] = "testcalldvbk"
	e["secondcall"] = "string4"
	e["multiselect"] = []string{"string2", "string4"}
	e["boolconfirm"] = true
	questions, err := values.Generate(e)
	fmt.Printf("%#v\n", values.Questions[0].MetaData)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	err = survey.Ask(questions, &e)
	fmt.Println(err)
}
