package surveycompose

import "github.com/zanven42/survey"

type inputType struct {
}

func (i inputType) GenerateQuestion(q Question) *survey.Question {
	qs := &survey.Question{
		Name: q.Name,
		Prompt: &survey.Input{
			Message: q.Message,
			Default: q.Default,
			Help:    q.Help,
		},
		// Validate:     "",
		// Transform:    "",
		SkipChildren: false,
		ParentName:   q.ParentName,
	}
	return qs
}
