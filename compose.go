package surveycompose

import (
	"fmt"
	"io"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"

	"github.com/zanven42/survey"
	yaml "gopkg.in/yaml.v2"
)

func init() {
}

type DisableQuestionOn struct {
	Question string
	Value    interface{}
}
type ValidatorDesc struct {
	Name      string        `yaml:"name"`
	Arguments []interface{} `yaml:"arguments"`
}

type Prompt struct {
	Type    string
	Options []string
}

type MetaData struct {
	Key   string      `yaml:"key"`
	Value interface{} `yaml:"value"`
}
type Question struct {
	Name               string          `yaml:"name"`
	Message            string          `yaml:"message"`
	Help               string          `yaml:"help"`
	ParentName         string          `yaml:"parent_name"`
	Prompt             Prompt          `yaml:"prompt"`
	Default            string          `yaml:"default"`
	Validators         []ValidatorDesc `yaml:"validators"`
	Transformers       []string        `yaml:"transformers"`
	DisablequestionsOn interface{}     `yaml:"disable_questions_on"`
	DisableList        []string        `yaml:"disable_list"`
	MetaData           []MetaData      `yaml:"metadata"`
}
type Compose struct {
	Questions []Question
}

func (c Compose) GetMetaData(name string) ([]MetaData, error) {
	for _, v := range c.Questions {
		if v.Name == name {
			return v.MetaData, nil
		}
	}
	return nil, fmt.Errorf("Metadata not found")
}

func (c Compose) Generate(ans interface{}) ([]*survey.Question, error) {
	defaults := make(map[string]reflect.Value)
	if ans != nil {
		ansType := reflect.TypeOf(ans)
		switch ansType.Kind() {
		case reflect.Struct:
			for i := 0; i < ansType.NumField(); i++ {
				field := ansType.Field(i)
				defaults[strings.ToLower(field.Name)] = reflect.ValueOf(ans).Field(i)
			}
		case reflect.Map:
			if ansType.Key().Kind() == reflect.String {
				v := reflect.ValueOf(ans)
				for _, k := range v.MapKeys() {
					value := v.MapIndex(k)
					if value.Kind() == reflect.Interface {
						value = reflect.ValueOf(value.Interface())
					}
					defaults[strings.ToLower(k.String())] = value
				}
			}
		}
	}
	var qs []*survey.Question
	for _, q := range c.Questions {
		// get validators
		validators := []survey.Validator{}
		for _, val := range q.Validators {
			switch val.Name {
			case "required":
				validators = append(validators, survey.Required)
			case "minlength":
				if len(val.Arguments) > 0 {
					if ival, ok := val.Arguments[0].(int); ok {
						validators = append(validators, survey.MinLength(ival))
					}
				}
			case "maxlength":
				if len(val.Arguments) > 0 {
					if ival, ok := val.Arguments[0].(int); ok {
						validators = append(validators, survey.MaxLength(ival))
					}
				}
			}
		}

		transformers := []survey.Transformer{}
		for _, trans := range q.Transformers {
			switch strings.ToLower(trans) {
			case "toupper":
				transformers = append(transformers, survey.TransformString(strings.ToUpper))
			case "tolower":
				transformers = append(transformers, survey.ToLower)
			case "title":
				transformers = append(transformers, survey.Title)
			}
		}
		ques := &survey.Question{
			Name:         q.Name,
			Prompt:       nil,
			Validate:     survey.ComposeValidators(validators...),
			Transform:    survey.ComposeTransformers(transformers...),
			SkipChildren: false,
			ParentName:   q.ParentName,
			Disables:     q.DisableList,
			DisableValue: q.DisablequestionsOn,
		}
		switch strings.ToLower(q.Prompt.Type) {
		case "input":
			defaultMsg := q.Default
			if v, ok := defaults[strings.ToLower(q.Name)]; ok {
				if v.Kind() == reflect.String {
					defaultMsg = v.String()
				}
			}
			ques.Prompt = &survey.Input{
				Message: q.Message,
				Default: defaultMsg,
				Help:    q.Help,
			}
		case "select":
			defaultMsg := q.Default
			if v, ok := defaults[strings.ToLower(q.Name)]; ok {
				if v.Kind() == reflect.String {
					defaultMsg = v.String()
				}
			}
			ques.Prompt = &survey.Select{
				Message:       q.Message,
				Options:       q.Prompt.Options,
				Default:       defaultMsg,
				Help:          q.Help,
				PageSize:      0,
				VimMode:       true,
				FilterMessage: "",
			}
		case "multiselect":
			var defaultMsg []string
			if q.Default != "" {
				defaultMsg = strings.Split(q.Default, ",")
			}
			if v, ok := defaults[strings.ToLower(q.Name)]; ok {
				if v.Len() == 0 {
					continue
				}
				if v.Index(0).Kind() == reflect.String {
					slice := make([]string, v.Len())
					for i := 0; i < v.Len(); i++ {
						slice[i] = v.Index(i).String()
					}
					defaultMsg = slice
				}

			}
			ques.Prompt = &survey.MultiSelect{
				Message:       q.Message,
				Options:       q.Prompt.Options,
				Default:       defaultMsg,
				Help:          q.Help,
				PageSize:      0,
				VimMode:       true,
				FilterMessage: "",
			}
		case "confirm":
			var defaultMsg bool
			if q.Default != "" {
				defaultMsg, _ = strconv.ParseBool(q.Default)
			}
			if v, ok := defaults[strings.ToLower(q.Name)]; ok {
				if v.Kind() == reflect.Bool {
					defaultMsg = v.Bool()
				}
			}
			ques.Prompt = &survey.Confirm{
				Message: q.Message,
				Default: defaultMsg,
				Help:    q.Help,
			}
		default:
			return nil, fmt.Errorf("Failed to find Question Type: %s, for question: %s\n", q.Prompt.Type, q.Name)

		}
		qs = append(qs, ques)
	}
	return qs, nil
}

func SetDefaults(questions []*survey.Question, answers interface{}) {
	for _, q := range questions {
		if q == nil {
			continue
		}

	}
}

func New(r io.Reader) (Compose, error) {
	if r == nil {
		return Compose{}, fmt.Errorf("Nil reader provided")
	}
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return Compose{}, err
	}
	comp := Compose{}
	err = yaml.Unmarshal(data, &comp)
	return comp, err
}
